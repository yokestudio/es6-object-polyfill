/**
 * Polyfills for ES-6 Object functions
 * - Object.assign()
 * - Object.is()
 * - Object.setPrototypeOf()
 *
 * Note: Object.getOwnPropertySymbols() not polyfilled as it only makes sense with Symbols,
 *       which is an ES6 feature that cannot be polyfilled correctly and easily.
 */

if (!Object.assign) {
	Object.defineProperty(Object, 'assign', {
		enumerable: false,
		configurable: true,
		writable: true,
		value: function(target) {
			'use strict';
			if (typeof target === 'undefined' || target === null) {
				throw new TypeError('Cannot convert first argument to object');
			}

			var to = Object(target);
			for (var i = 1; i < arguments.length; i++) {
				var nextSource = arguments[i];
				if (nextSource === undefined || nextSource === null) {
					continue;
				}
				nextSource = Object(nextSource);

				var keysArray = Object.keys(Object(nextSource));
				for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
					var nextKey = keysArray[nextIndex];
					var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
					if (desc !== undefined && desc.enumerable) {
						to[nextKey] = nextSource[nextKey];
					}
				}
			}
			return to;
		}
	});
}
if (!Object.is) {
	Object.is = function(x, y) {
		// SameValue algorithm
		if (x === y) { // Steps 1-5, 7-10
			// Steps 6.b-6.e: +0 != -0
			return x !== 0 || 1 / x === 1 / y;
		}
		else {
			// Step 6.a: NaN == NaN
			return x !== x && y !== y;
		}
	};
}

Object.setPrototypeOf = Object.setPrototypeOf || function(obj, proto) {
	obj.__proto__ = proto;
	return obj; 
}