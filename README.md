# ES6 Object Polyfill #

A JavaScript polyfill for ES6 Object functions.

Mostly a collection of polyfilled functions retrieved from [MDN](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object).

### Set Up ###

1. Download the latest release (as of 10 Jul 2015, v1.0.0).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="es6-object-polyfill-1.0.0.min.js"></script>`

### API ###

Refer to [MDN](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object).

### FAQ ###

1. Does this polyfill have any bugs/limitations?

> `Object.getOwnPropertySymbols()` is not polyfilled as it only makes sense with Symbols, which is an ES6 feature that cannot be polyfilled correctly and easily.
>
> If you find any other bugs/limitations, please let us know.
   
### Contact ###

* Email us at <yokestudio@hotmail.com>.